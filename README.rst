dotfiles
========

| dotfiles is a repository, that should go into the user's $HOME.
| It features configurations for zsh and many others.

Requirements
------------
Some requirements have to be met to make things work smoothly.
As I'm using |arch_linux|, all links are Arch specific.

zsh
***
I make extensive use of |zsh| whenever I can. The following plugins/extensions are needed for this repository to work.

* |zsh-syntax-highlighting| (|aur|)
* |zsh-completions|
* |zsh-history-substring-search-git| (|aur|)

autorandr
*********
For the screen setup scripts to work |autorandr| is needed.

Installation
------------
If the $HOME is that of a new user, one can simple init an empty repository and set its remote to the correct path:

  .. code:: bash

    git init
    # readonly link
    git remote add origin git://sleepmap.de/config/dotfiles
    # read/write link
    git remote add origin git@sleepmap.de:config/dotfiles
    git fetch
    git checkout -t origin/master

vim
***
|vim-addon-manager| will install all activated plugins.
While many need customization (which is done in .vimrc), |youcompleteme| needs compilation:

  .. code:: bash

    cd ~/.vim/vim-addons/github-Valloric-YouCompleteMe.git/
    git clean -f
    git pull
    git submodule update --recursive --init
    ./install.py --all


Customization
-------------
The git configuration in *.config/git/config* comes with no **[user]** fields set.
Those can be added in a separate *.gitconfig* file, e.g.:

  .. code:: ini

    [user]
      email = your@e-mail.tld
      name = Some Name
      signingkey = your pgp key


.. |autorandr| raw:: html

  <a href="https://aur.archlinux.org/packages/autorandr-git" target="_blank">autorandr</a>

.. |zsh| raw:: html

  <a href="https://www.archlinux.org/packages/extra/x86_64/zsh/" target="_blank">zsh</a>

.. |zsh-completions| raw:: html

  <a href="https://www.archlinux.org/packages/community/any/zsh-completions/" target="_blank">zsh-completions</a>

.. |zsh-syntax-highlighting| raw:: html

  <a href="https://aur.archlinux.org/packages/zsh-syntax-highlighting-git/" target="_blank">zsh-syntax-highlighting</a>

.. |zsh-history-substring-search-git| raw:: html

  <a href="https://aur.archlinux.org/packages/zsh-history-substring-search-git/" target="_blank">zsh-history-substring-search</a>

.. |aur| raw:: html

  <a href="https://aur.archlinux.org/" target="_blank">AUR</a>

.. |arch_linux| raw:: html

  <a href="https://archlinux.org/" target="_blank">Arch Linux</a>

.. |youcompleteme| raw:: html

  <a href="https://github.com/Valloric/YouCompleteMe" target="_blank">YouCompleteMe</a>

.. |vim-addon-manager| raw:: html

  <a href="https://github.com/MarcWeber/vim-addon-manager" target="_blank">VAM</a>
