#!/usr/bin/env zsh

pkg_sign() {
  # sign one or more packages with the key setup in makepkg.conf
  local _pkg_list=()
  local _arg _pkg

  # get GPGKEY from makepkg.conf
  if [[ -f /etc/makepkg.conf ]]; then
    source /etc/makepkg.conf 2>/dev/null 1>&2
  fi
  if [[ -f "${HOME}/.makepkg.conf" ]]; then
    source "${HOME}/.makepkg.conf" 2>/dev/null 1>&2
  fi
  if [[ -z "${GPGKEY}" ]]; then
    1>&2 printf "No GPGKEY is setup in makepkg.conf!\n"
    return 1
  fi

  # check whether file(s) exists or globs exist
  for _arg in "$@"; do
    if [[ -f "${_arg}" ]] && [[ "${_arg}" == *.pkg.tar.zst ]]; then
      _pkg_list+=( "${_arg}" )
    fi
    if [[ -n *"${_arg}"*.pkg.tar.zst(#qN) ]]; then
      _pkg_list+=( *"${_arg}"*.pkg.tar.zst )
    fi
  done

  # sign package(s)
  if (( ${#_pkg_list} > 0 )); then
    for _pkg in "${_pkg_list[@]}"; do
      gpg --detach-sign --local-user "${GPGKEY}" "$_pkg"
    done
    return 0
  else
    1>&2 printf "No package can be found for signing!\n"
    return 1
  fi
}

pkg_add() {
  # sign one or more packages with the key setup in makepkg.conf
  local _pkg_list=()
  local _arg

  # get GPGKEY from makepkg.conf
  if [[ -f /etc/makepkg.conf ]]; then
    source /etc/makepkg.conf 2>/dev/null 1>&2
  fi
  if [[ -f "${HOME}/.makepkg.conf" ]]; then
    source "${HOME}/.makepkg.conf" 2>/dev/null 1>&2
  fi
  if [[ -z "${GPGKEY}" ]]; then
    1>&2 printf "No GPGKEY is setup in makepkg.conf!\n"
    return 1
  fi

  # check whether file(s) exists or globs exist
  for _arg in "$@"; do
    if [[ -f "${_arg}" ]] && [[ "$_arg" == *.pkg.tar.zst ]]; then
      _pkg_list+=( "${_arg}" )
    fi
    if [[ -n *"${_arg}"*.pkg.tar.zst(#qN) ]]; then
      _pkg_list+=( *"${_arg}"*.pkg.tar.zst )
    fi
  done

  # add package(s) to repo database
  if (( ${#_pkg_list} > 0 )); then
    repo-add -R -s -k "${GPGKEY}" "$(dirname "${_pkg_list[1]}")"/*.db.tar.gz "${_pkg_list[@]}"
    return 0
  else
    1>&2 printf "No packages to add can be found!\n"
    return 1
  fi
}

sshfs_mount() {
  mkdir -p "${HOME}/mounts/$1"
  sshfs -C -F "${HOME}/.ssh/config" "$1": "${HOME}/mounts/$1"
}

sshfs_umount() {
  fusermount3 -u "${HOME}/mounts/$1"
}

nvc() {
  local config
  if [[ -z "$1" ]]; then
    1>&2 printf "A repository name needs to be specified as the first argument.\n"
    return 1
  fi
  config="${HOME}/.config/nvchecker/$1.toml"
  if [[ ! -f "${config}" ]]; then
    1>&2 printf "The configuration does not exist: %s\n" "${config}"
    return 1
  fi
  if ! command -v nvchecker > /dev/null; then
    1>&2 printf "The required application 'nvchecker' can not be found.\n"
    return 1
  fi
  nvchecker -c "${config}"
}

nvt() {
  local config_file package repo state_dir
  if [[ -z "$1" ]]; then
    1>&2 printf "A repository name needs to be specified as the first argument.\n"
    return 1
  fi
  if [[ -z "$2" ]]; then
    1>&2 printf "A package name needs to be specified as the second argument.\n"
    return 1
  fi

  repo="$1"
  config_file="$HOME/.config/nvchecker/$repo.toml"
  if [[ ! -f "$config_file" ]]; then
    1>&2 printf "The configuration does not exist: %s\n" "$config_file"
    return 1
  fi

  package="$2"
  if ! grep "$package" "$config_file" > /dev/null; then
    1>&2 printf "The package %s can not be found in the configuration: %s\n" "$package" "$config_file"
    return 1
  fi

  if ! command -v nvtake > /dev/null; then
    1>&2 printf "The required application 'nvtake' can not be found.\n"
    return 1
  fi
  nvtake -c "$config_file" "$package"

  state_dir="$HOME/.local/state/nvchecker/"
  git -C "$state_dir" add "$repo.*.json*"
  git -C "$state_dir" commit -s -m "Update $package"
  git -C "$state_dir" pull --rebase
  git -C "$state_dir" push
}
