# make git auto completion faster by favoring local files
__git_files () {
  _wanted files expl 'local files' _files
}
