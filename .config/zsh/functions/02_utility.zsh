# Makes a directory and changes to it.
function mkdcd {
  [[ -n "$1" ]] && mkdir -p "$1" && builtin cd "$1"
}

# Changes to a directory and lists its contents.
function cdls {
  builtin cd "$argv[-1]" && ls "${(@)argv[1,-2]}"
}

# Pushes an entry onto the directory stack and lists its contents.
function pushdls {
  builtin pushd "$argv[-1]" && ls "${(@)argv[1,-2]}"
}

# Pops an entry off the directory stack and lists its contents.
function popdls {
  builtin popd "$argv[-1]" && ls "${(@)argv[1,-2]}"
}

# Prints columns 1 2 3 ... n.
function slit {
  awk "{ print ${(j:,:):-\$${^@}} }"
}

# Finds files and executes a command on them.
function find-exec {
  find . -type f -iname "*${1:-}*" -exec "${2:-file}" '{}' \;
}

# Displays user owned processes status.
function psu {
  ps -U "${1:-$LOGNAME}" -o 'pid,%cpu,%mem,command' "${(@)argv[2,-1]}"
}

# Highlights make output.
function make {
  if (( $+commands[colormake] )); then
    colormake "$@"
  else
    command make "$@"
  fi
}

# Lists the contents of archives.
function lsarchive {
  while (( $# > 0 )); do
    if [[ ! -s "$1" ]]; then
      print "$0: file not valid: $1" >&2
      shift
      continue
    fi
    case "$1:l" in
      (*.tar.gz|*.tgz) tar t${verbose:+v}vzf "$1" ;;
      (*.tar.bz2|*.tbz|*.tbz2) tar t${verbose:+v}jf "$1" ;;
      (*.tar.xz|*.txz) tar --xz --help &> /dev/null \
        && tar --xz -t${verbose:+v}f "$1" \
        || xzcat "$1" | tar t${verbose:+v}f - ;;
      (*.tar.zma|*.tlz) tar --lzma --help &> /dev/null \
        && tar --lzma -t${verbose:+v}f "$1" \
        || lzcat "$1" | tar x${verbose:+v}f - ;;
      (*.tar) tar t${verbose:+v}f "$1" ;;
      (*.zip) unzip -l${verbose:+v} "$1" ;;
      (*.rar) unrar &> /dev/null \
        && unrar ${${verbose:+v}:-l} "$1" \
        || rar ${${verbose:+v}:-l} "$1" ;;
      (*.7z) 7za l "$1" ;;
      (*)
        print "$0: cannot list: $1" >&2
        success=1
      ;;
    esac
    shift
  done
}
