#
# Commands executed by zsh on logout.
#

if [ $(id -u) -eq 0 ]; then
  clear
fi
