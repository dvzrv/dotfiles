#
# Executes commands at login pre-zshrc.

#
# Paths
#

# Ensure path arrays do not contain duplicates.
typeset -gU cdpath fpath mailpath path

# Set the the list of directories that cd searches.
# cdpath=(
#   $cdpath
# )

# Set the list of directories that Zsh searches for programs.
path=(
  /usr/local/{bin,sbin}
  $path
)


#
# Temporary Files
#

if [[ ! -d "$TMPDIR" ]]; then
  if [[ $USER == "root" ]]; then
    export TMPDIR="/tmp/$USER"
  else
    export TMPDIR="$XDG_RUNTIME_DIR"
  fi
  mkdir -p -m 700 "$TMPDIR"
fi

TMPPREFIX="${TMPDIR%/}/zsh"
if [[ ! -d "$TMPPREFIX" ]]; then
  mkdir -p "$TMPPREFIX"
fi
