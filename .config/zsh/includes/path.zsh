#!/usr/bin/zsh

# cargo
[ -d "${HOME}/.cargo/bin" ] && PATH="${HOME}/.cargo/bin:${PATH}"
# pyenv
[ -d "${HOME}/.local/bin" ] && PATH="${HOME}/.local/bin:${PATH}"
[ -d "${HOME}/.local/share/pyenv/shims" ] && PATH="${HOME}/.local/share/pyenv/shims:${PATH}"
# RPi
[ -d "/opt/vc/bin" ] && PATH="${PATH}:/opt/vc/bin"
[ -d "/opt/vc/sbin" ] && PATH="${PATH}:/opt/vc/sbin"
# user bin
[ -d "${HOME}/bin" ] && PATH="${HOME}/bin:${PATH}"
# Debilian
[ -d "/usr/sbin" ] && [ ! -L "/usr/sbin" ] && PATH="${PATH}:/usr/sbin"
# whackOS
[ -d "/usr/local/opt/coreutils/libexec/gnubin" ] && PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
[ -d "/usr/local/opt/gnu-sed/libexec/gnubin" ] && PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
[ -d "/usr/local/opt/gnu-tar/libexec/gnubin" ] && PATH="/usr/local/opt/gnu-tar/libexec/gnubin:$PATH"
[ -d "/usr/local/opt/gnu-which/libexec/gnubin" ] && PATH="/usr/local/opt/gnu-which/libexec/gnubin:$PATH"
[ -d "/usr/local/opt/findutils/libexec/gnubin" ] && PATH="/usr/local/opt/findutils/libexec/gnubin:$PATH"
[ -d "/usr/local/opt/grep/libexec/gnubin" ] && PATH="/usr/local/opt/grep/libexec/gnubin:$PATH"
