# < FUNCTIONS

## PDF/LATEX
  function mkpdf()
  {
    pdflatex -shell-escape  $1
    # latex --output-format=pdf $1
    name=`echo $1 | sed 's/.tex//'`
    EXT=(aux log)
    for i in $EXT;
    do
      rm -v $name.$i
    done
  }

  function gsmerge()
  {
    target=$1
    echo "target: $target"
    sleep 1
    shift

    /usr/bin/gs \
      -sDEVICE=pdfwrite \
      -dCompatibilityLevel=1.4 \
      -dNOPAUSE \
      -dBATCH \
      -sPAPERSIZE=a4 \
      -sOUTPUTFILE=$target $*
  }

  function gsmerge_medium()
  {
    target=$1
    echo "target: $target"
    sleep 1
    shift

    /usr/bin/gs \
      -sDEVICE=pdfwrite \
      -dPDFSETTINGS=/ebook \
      -dCompatibilityLevel=1.4 \
      -dNOPAUSE \
      -dBATCH \
      -sPAPERSIZE=a4 \
      -sOutputFile=$target $*
  }

## USABILITY
  function lvim()
  {
    noglob vim $(echo $1 | awk -F":" '{ print $1" +"$2  }' )
  }

  function mkcd()
  {
    mkdir $1
    cd $1
  }

  function cpwd()
  {
    pwd >! /tmp/pwd
  }

  function ppwd()
  {
    cd "`cat /tmp/pwd`"
  }

  function cp2wd()
  {
    cp $@ "`cat /tmp/pwd`"
  }
  function cpmk() {
    DIR=${*: -1}
    [ ! -d $DIR ] && mkdir -p $DIR
    cp $*

  }

  cpmd5(){
    md5sum $1|cut -d ' ' -f 1|cpx
  }


function publish() {
  [ -f $1 ] || return

  DIR=`dirname $1`
  FILE=`basename $1`
  mute pushd $DIR
  scp $FILE pool:public_html/
  echo "http://www-pool.math.tu-berlin.de/~runge/$FILE"
  echo "http://www-pool.math.tu-berlin.de/~runge/$FILE"|cpx
  mute popd
}

function incognichrome {
  chromium --incognito
}

function inproxychrome {
  chromium --incognito --proxy-server=apu-serve:8123 --user-data-dir="$HOME/.config/chromium-proxy"
}

## ZSH
refresh() {
  source $HOME/.zshrc
}


## compress stuff
tar_tgz() {
  tar cvfz $1.tgz $1
}
tar_tbz() {
  tar cvfj $1.tbz $1
}
tar_tlz() {
  tar --lzma -cvf $1.tlz $1
}
tar_xz() {
  tar cvfJ $1.tar.xz $1
}

#Decompress any given compressed file
ex() {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)
        tar xvjf $1
        ;;
      *.tar.gz)
        tar xvzf $1
        ;;
      *.tar.lz)
        tar xvf $1
        ;;
      *.bz2)
        bunzip2 $1
        ;;
      *.deb)
        ar vx $1
        ;;
      *.rar)
        unrar x $1
        ;;
      *.pgp)
        gpg -o ${1%".pgp"} -d $1
        ;;
      *.gpg)
        gpg -o ${1%".gpg"} -d $1
        ;;
      *.asc)
        gpg -o ${1%".asc"} -d $1
        ;;
      *.gz)
        gunzip $1
        ;;
      *.tar)
        tar xvf $1
        ;;
      *.tbz2)
        tar xvjf $1
        ;;
      *.tgz)
        tar xvzf $1
        ;;
      *.whl)
        unzip "$1"
        ;;
      *.xz)
        tar xvJf $1
        ;;
      *.zip)
        unzip $1
        ;;
      *.zst)
        tar --zstd -xvf $1 --force-local
        ;;
      *.Z)
        uncompress $1
        ;;
      *.7z)
        7z x $1
        ;;
      *)
        echo "Don't know how to extract '$1'..." 
        ;;
    esac
  else
    echo "'$1' is not a valid file to extract!"
  fi
}

ex2() {
  if [ -f $2 ] ;then
    echo "Extracting to '$2'"
    local out = $2
  else
    echo "Extracting to '$( pwd )'"
    local out = $( pwd )
  fi
  if [ -f $1 ] ;then
    echo "'$1'"
  else
    echo "'$1' is not a valid file!"
  fi
}

lstar() {
  if [ -f "$1" ] ; then
    case "$1" in
      *.deb)
        ar -tv "$1"
        ;;
      *.bz2)
        tar -tvf "$1" --force-local
        ;;
      *.gz)
        tar -tvf "$1" --force-local
        ;;
      *.lz)
        tar -tvf "$1" --force-local
        ;;
      *.rar)
        unrar lb "$1"
        ;;
      *.tar)
        tar -tvf "$1" --force-local
        ;;
      *.tbz2)
        tar -tvf "$1" --force-local
        ;;
      *.tgz)
        tar -tvf "$1" --force-local
        ;;
      *.whl)
        unzip -l "$1"
        ;;
      *.xz)
        tar -Jtvf "$1" --force-local
        ;;
      *.zip)
        unzip -l "$1"
        ;;
      *.Z)
        uncompress $1
        ;;
      *.7z)
        7z l "$1"
        ;;
      *.zst)
        tar --zstd -tvf "$1" --force-local
        ;;
      *)
        echo "Don't know how to look into $1"
        exit 1
        ;;
    esac
  else
    echo "'$1' is not a file!"
    exit 1
  fi
}

## RSYNC

backupinfo() {
  local options=(
    --delete
    --dry-run
    --exclude 'System Volume Information'
    --exclude 'lost+found'
    --exclude '.DS_Store'
    --exclude '.Trash-*'
    --exclude '.snapshots'
    --exclude '.thumbs'
    --exclude '$RECYCLEBIN'
    --group
    --human-readable
    --ignore-existing
    --owner
    --perms
    --progress
    --recursive
    --secluded-args
    --size-only
    --times
    --verbose
  )

  rsync "${options[@]}" "$1" "$2"
  echo "NOTE: path/to/folder/ /path/to/other/folder"
}

backupfolder() {
  local options=(
    --delete
    --exclude 'System Volume Information'
    --exclude 'lost+found'
    --exclude '.DS_Store'
    --exclude '.Trash-*'
    --exclude '.snapshots'
    --exclude '.thumbs'
    --exclude '$RECYCLEBIN'
    --group
    --human-readable
    --ignore-existing
    --owner
    --perms
    --progress
    --recursive
    --secluded-args
    --size-only
    --times
    --verbose
  )

  rsync "${options[@]}" "$1" "$2"
}

# journalctl

# show messages of specified system units
function sdsjfu(){
  local journal_cmd="sudo journalctl --system -a"
  eval ${journal_cmd} $(echo $@ | sed 's/\ /\ -u\ /g; s/^/\ -u\ /')
}

# follow messages of specified system units
function sdsjfu(){
  local journal_cmd="sudo journalctl --system -af"
  eval ${journal_cmd} $(echo $@ | sed 's/\ /\ -u\ /g; s/^/\ -u\ /')
}

# follow messages of specified system units for this boot
function sdsjbfu(){
  local journal_cmd="sudo journalctl --system -b -af"
  eval ${journal_cmd} $(echo $@ | sed 's/\ /\ -u\ /g; s/^/\ -u\ /')
}

# follow messages of specified user units
function sdujfu(){
  local journal_cmd="journalctl --user -af"
  eval ${journal_cmd} $(echo $@ | sed 's/\ /\ --user-unit\ /g; s/^/\ --user-unit\ /')
}

# follow messages of specified user units for this boot
function sdujbu(){
  local journal_cmd="journalctl --user -a -b"
  #TODO: Check if first argument is a number, then use it for -b
  eval ${journal_cmd} $(echo $@ | sed 's/\ /\ --user-unit\ /g')
}

# show messages of specified system units since and until a specified time
function sdsjSUu(){
  local journal_cmd="sudo journalctl --system -a -S"
  local args=(${@})
  journal_cmd="$journal_cmd $args[1] -U $args[2]"
  args=(${args:2})
  eval ${journal_cmd} $(echo $args | sed 's/\ /\ -u\ /g; s/^/\ -u\ /')
}

# show messages of specified system units since a specified time
function sdsjSu(){
  local journal_cmd="sudo journalctl --system -a -S"
  local args=(${@})
  journal_cmd="$journal_cmd $args[1]"
  args=(${args:1})
  eval ${journal_cmd} $(echo $args | sed 's/\ /\ -u\ /g; s/^/\ -u\ /')
}

# show messages of specified user units since and until a specified time
function sdujSUu(){
  local journal_cmd="journalctl --user -a -S"
  local args=(${@})
  journal_cmd="$journal_cmd $args[1] -U $args[2]"
  args=(${args:2})
  eval ${journal_cmd} $(echo $args | sed 's/\ /\ --user-unit\ /g; s/^/\ --user-unit\ /')
}

# show messages of specified user units since a specified time
function sdujSu(){
  local journal_cmd="journalctl --user -a -S"
  local args=(${@})
  journal_cmd="$journal_cmd $args[1]"
  args=(${args:1})
  eval ${journal_cmd} $(echo $args | sed 's/\ /\ --user-unit\ /g; s/^/\ --user-unit\ /')
}

# show kernel messages since and until a specified time
function sdsjkSU(){
  local journal_cmd="sudo journalctl --system -a -k -S"
  local args=(${@})
  journal_cmd="$journal_cmd $args[1] -U $args[2]"
  eval ${journal_cmd}
}

# show kernel messages since a specified time
function sdsjkS(){
  local journal_cmd="sudo journalctl --system -a -k -S"
  local args=(${@})
  journal_cmd="$journal_cmd $args[1]"
  eval ${journal_cmd}
}


function pasters() {
  local file="${1:-/dev/stdin}"
  curl --data-binary @"${file}" https://paste.rs
}

function pkg_keyid_lookup() {
  # lookup all packages signed by a given PGP key ID (either 16 or 40 chars long)
  local key_id="$1"
  local key_id_length=${#key_id}

  if (( $key_id_length == 40 )); then
    key_id=${key_id[25,40]}
    key_id_length=${#key_id}
  fi

  if (( $key_id_length != 16 )); then
    printf "Key ID is not 40 or 16 chars long: %s\n" $key_id
    return 1
  fi

  pacman -Sii | awk -v signature=$key_id 'BEGIN {RS=""; FS="\n"} {if ($0 ~ signature && $2 ~ /Name/) {split($2, m, ":"); gsub(/ /, "", m[2]); print m[2]}}'
}
# FUNCTIONS >
