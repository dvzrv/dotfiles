## OVERRIDE KEY-BINDINGS
bindkey -s '\e,' '..\n'                             # [Esc-,] - run command: .. (up directory)
bindkey '\e.' insert-last-word                      # [Esc-.] - insert last word
#bindkey '^[Oc' forward-word                         # [Ctrl-RightArrow] - move forward one word
#bindkey '^[OC' forward-word                         # [Ctrl-RightArrow] - move forward one word
#bindkey '^[Od' backward-word                        # [Ctrl-LeftArrow] - move backward one word
#bindkey '^[OD' backward-word                        # [Ctrl-LeftArrow] - move backward one word

## URXVT & TMUX
case $TERM in
  rxvt-unicode-256color)
    bindkey '^[Oc' forward-word       # [Ctrl-RightArrow] - move forward one word
    bindkey '^[Od' backward-word      # [Ctrl-LeftArrow]  - move backward one word
  ;;
  screen-256color)
    bindkey '^[[1;5C' forward-word       # [Ctrl-RightArrow] - tmux: move forward one word
    bindkey '^[[1;5D' backward-word      # [Ctrl-LeftArrow]  - tmux: move backward one word
  ;;
esac

## NEW KEY-BINDINGS
bindkey '^K' kill-line
