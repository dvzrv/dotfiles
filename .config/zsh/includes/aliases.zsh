# suffix aliases
alias -s pdf='mupdf'
alias -s ps='mupdf'
alias -s jpg='feh'
alias -s png='feh'
alias -s gif='viewnior'
alias -s gz='tar xvfz'
alias -s tgz='tar xvfz'
alias -s bz2='tar xvfj'
alias -s tbz='tar xvfj'
alias -s tlz='tar --lzma -xvf'
alias -s rar='unrar x'
alias -s tex='mkpdf'

# global aliases
alias -g ...='../..'
alias -g ....='../../..'
alias -g .....='../../../..'
alias -g C='| wc -l'
alias -g H='| head'
alias -g L="| less"
alias -g N="| /dev/null"
alias -g S='| sort'
alias -g G='| grep'

# ls
alias ls='ls --group-directories-first --color=auto'
alias ll="ls -lh"
alias la="ls -a"
alias lal="ls -alhv"
alias lt='ls -tr'
alias ll='ls -l'
alias llh='ls -lh'
alias llt='ls -ltr'
alias la='ls -A'
alias lla='ls -lA'
alias lsfw='ls -lah /dev|grep fw'
alias lsvid='ls -lah /dev|grep video'

alias less='less -F -g -i -M -R -S -w -X -z -4'
#zsh

# Lists the ten most used commands.
alias history-stat="history 0 | awk '{print \$2}' | sort | uniq -c | sort -n -r | head"

alias d='dirs -v'
for index ({1..9}) alias "$index"="cd +${index}"; unset index

#MPLAYER
alias mplayer='mplayer -ao jack,pulse,alsa'
if pgrep Xwayland > /dev/null; then
  alias mpv='mpv --audio-channels=stereo --cache=yes --gpu-context=wayland -ao=jack,pulse,alsa --sub-auto=fuzzy --jack-port="system:playback_*"'
else
  alias mpv='mpv --audio-channels=stereo --cache=yes -ao=jack,pulse,alsa --sub-auto=fuzzy --jack-port="system:playback_*"'
fi
if [[ "$VENDOR" == "apple" ]]; then
  alias mpv='mpv --sub-auto=fuzzy'
fi
##DU
alias du1='du -hd1'

##FEH
alias feh='feh -Z -g 1920x1200 -b "black" -S "filename"'

##GIT
alias gsi='git submodule update --init --recursive'
alias gsu='git submodule update --recursive'

##TREE
alias tree='tree -aC'
#grep aliases (with excluded directories)
alias grepdot='grep -rI --exclude-dir=".steam" --exclude-dir=".local" --exclude-dir=".thunderbird" --exclude-dir=".mozilla" --exclude-dir="git" --exclude-dir=".rvm" --exclude-dir=".log" --exclude-dir=".dropbox-dist" --exclude-dir=".config" '


##SYSTEMD
#tree
alias psc='ps xawf -eo pid,user,cgroup,args'
alias treeg='tree -d /cgroup/systemd/'
alias sdptree='tree -aC /usr/lib/systemd/'
alias sdstree='tree -aC /etc/systemd/'
alias sdutree='tree -aC $HOME/.config/systemd/'
#journalctl
alias sdsj='sudo journalctl --system -a' #show the entire system journal
alias sdsjf='sudo journalctl --system -af' #follow all system messages
alias sdsjb='sudo journalctl --system -ab' #Show system messages from specific boot
alias sdsjbf='sudo journalctl --system -ab -f' #follow only messages from this boot
alias sdsju='sudo journalctl --system -af -u' #follow messages of only the specified unit
alias sdsjk='journalctl -k' #show dmesg
alias sdsjkb='journalctl -kb' #show dmesg from specific boot
alias sdsjkf='journalctl -kf' #follow current dmesg
alias sdsjlb='journalctl --list-boots' #list all boots
alias sdsjp='sudo journalctl --system _PID='  #show all messages of specified system process (PID)
alias sdjgrep='sudo journalctl -an1000 |grep ' #Use a grep command to search through journal
alias sduj='journalctl --user -a'
alias sdujf='journalctl --user -af'
alias sdujb='journalctl --user -ab'
alias sdujbf='journalctl --user -afb'
alias sdujb='journalctl --user -ab'
alias sdujp='journalctl --user _PID='  #show all messages of specified user process (PID)
#systemd system services
alias sdsedit='sudo systemctl edit'
alias sdsstart='sudo systemctl start'
alias sdsstop='sudo systemctl stop'
alias sdsrestart='sudo systemctl restart'
alias sdsreload='sudo systemctl reload'
alias sdsenable='sudo systemctl enable'
alias sdsdisable='sudo systemctl disable'
alias sdsreenable='sudo systemctl reenable'
alias sdsisactive='sudo systemctl is-active'
alias sdsisenabled='sudo systemctl is-enabled'
alias sdsstat='sudo systemctl status -a -l'
alias sdswants='sudo systemctl show -p "Wants"'
alias sdswantedby='sudo systemctl show -p "WantedBy"'
alias sdsrequires='sudo systemctl show -p "Requires"'
alias sdsrequiredby='sudo systemctl show -p "RequiredBy"'
alias sdsconflicts='sudo systemctl show -p "Conflicts"'
alias sdsconflictedby='sudo systemctl show -p "ConflictedBy"'
alias sdsbefore='sudo systemctl show -p "Before"'
alias sdsafter='sudo systemctl show -p "After"'
alias sdst='sudo systemctl --system list-timers'
alias sdss='sudo systemctl --system list-sockets'
alias sdsu='sudo systemctl --system list-units'
alias sdsuf='sudo systemctl --system list-unit-files'
alias sdsdep='sudo systemctl --system list-dependencies'
alias sdscat='systemctl --system cat'
alias sdsreload='sudo systemctl daemon-reload'
alias sds='sudo systemctl -a'
alias sdsfail='sudo systemctl -a -l --no-legend --state=failed'
alias sdsmask='sudo systemctl mask'
alias sdsunmask='sudo systemctl unmask'
#systemd user services
alias sduedit='systemctl --user edit'
alias sdustart='systemctl --user start'
alias sdustop='systemctl --user stop'
alias sdurestart='systemctl --user restart'
alias sdureload='systemctl --user reload'
alias sduenable='systemctl --user enable'
alias sdudisable='systemctl --user disable'
alias sdureenable='systemctl --user reenable'
alias sduisactive='systemctl --user is-active'
alias sduisenabled='systemctl --user is-enabled'
alias sdustat='systemctl --user status -a -l'
alias sduwants='systemctl --user show -p "Wants"'
alias sduwantedby='systemctl --user show -p "WantedBy"'
alias sdurequires='systemctl --user show -p "Requires"'
alias sdurequiredby='systemctl --user show -p "RequiredBy"'
alias sduconflicts='systemctl --user show -p "Conflicts"'
alias sduconflictedby='systemctl --user show -p "ConflictedBy"'
alias sdubefore='systemctl --user show -p "Before"'
alias sduafter='systemctl --user show -p "After"'
alias sdut='systemctl --user list-timers'
alias sdus='systemctl --user list-sockets'
alias sduu='systemctl --user list-units'
alias sdudep='systemctl --user list-dependencies'
alias sducat='systemctl --user cat'
alias sdureload='systemctl --user daemon-reload'
#alias sdu='systemctl --user'
alias sdufail='systemctl --user -a -l --no-legend --state=failed'
alias sdumask='systemctl --user mask'
alias sduunmask='systemctl --user unmask'
#poweroff/reboot/isolate
alias sdiso='sudo systemctl isolate '
alias sdreboot='sudo systemctl reboot'
alias sdpoweroff='sudo systemctl poweroff'
alias sdsuspend='sudo systemctl suspend'
#systemd bootchart
alias sda='systemd-analyze'
alias sdab='systemd-analyze blame'
if [ -x /usr/bin/xsvg ]; then
  alias sdap='xsvg $HOME/cloud/photos/systemd/sdap-$HOST.svg'
fi

##PACMAN
alias paclid="sudo pacman -Qi | sed '/^Depends On/,/^Required By/{ s/^Required By.*$//; H; d }; /^Name/!d; /^Name/{ n;x;}'| sed '/^$/s//==================================================================================/'"
alias pacsize="pacman -Qi | awk '/^Name/ {pkg=\$3} /Size/ {print \$4\$5,pkg}' | sort -n"  #Show all packages by size
alias pacall="sudo pacman -Qi | awk '/^Name/ {pkg=\$3} /Version/ {ver=\$3} /Size/ {size=\$4\$5} {print pkg,ver,size}' | uniq"  #All packages uniquely with name, version, size
alias pacup='sudo pacman -Syu'  #Synchronize with repositories before upgrading packages that are out of date on the local system.
alias pac='sudo pacman -S'  #Install specific package(s) from the repositories
alias pacu='sudo pacman -U'  #Install specific package not from the repositories but from a file 
alias pacre='sudo pacman -R'  #Remove the specified package(s), retaining its configuration(s) and required dependencies
alias pacrem='sudo pacman -Rns'  #Remove specified package(s), its configuration(s) and unneeded depends
alias pacname="sudo pacman -Qi|grep Name|grep "  #Search through names of packages by string
alias pacri='pacman -Si'  #Display information about a given package in the repositories
alias pacrs='pacman -Ss'  #Search for package(s) in the repositories
alias pacli='pacman -Qi'  #Display information about a given package in the local database
alias pacls='pacman -Qs'  #Search for package(s) in the local database
alias pacorph='sudo pacman -Qdt'  #Search for orphan packages
alias paclal='pacman -Ql'  #List installed files of a given package
alias pacg='pacman -Sg'  #List all groups or all packages in specified group
alias pacclean='sudo pacman -Sc'  #Remove all packages from local repository, not installed atm, remove all unused repos
alias pacconf='sudo find /etc -type f \( -name "*.pacsave" -o -name "*.pacnew" \)'  #Search for .pacnew and .pacsave files
alias pacown='sudo pacman -Qo'  # Find out what package a give file belongs to
alias pacupd='sudo pacman -Sy && sudo abs'  #Update and refresh the local package and ABS databases against repositories
alias pacdep='sudo pacman -S --asdeps'  #Install given package(s) as dependencies of another package
alias pacmir='sudo pacman -Syy'  #Force refresh of all package lists after updating /etc/pacman.d/mirrorlist
alias packup='comm -23 <(pacman -Qeq|sort) <(pacman -Qmq|sort) > $HOME/Dropbox/sync/archlinux/pkglist-$HOST'  #Backup the current list of non-local packages
alias pacret='sudo pacman -S $(< $HOME/Dropbox/sync/archlinux/pkglist-$HOST)'  #Install from the backup list
alias pacredown='sudo pacman -S $(comm -23 <(pacman -Qeq) <(pacman -Qmq))'  #Redownload all installed packages (minus AUR)
alias pacdis='sudo pacman-disowned > /tmp/$USERNAME/non-pkg-$HOST'  #Getting a list of files not owned by any package
alias packimp='sudo pacman-key -r '  #Import a key ID to the pacman keyring
alias packsign='sudo pacman-key --lsign-key '  #Sign a key ID after importing it to the keyring
alias packref='sudo pacman-key --refresh-keys'  #Refresh the list of master keys

##XCLIP
alias cpx="xclip -selection clipboard -display :0"

##CD
alias cd.sds="cd /etc/systemd/system"
alias cd.sdp="cd /usr/lib/systemd/system"
alias cd.courses="cd $HOME/ownCloud/documents/university/tu-berlin/courses"
alias cd.backup="cd $HOME/ownCloud/backup"
alias cd.scpref="cd $HOME/.config/SuperCollider"

##NMCLI
alias nmup="sudo nmcli con up id"
alias nmdown="sudo nmcli con down id"
alias nmstat="sudo nmcli con show --active"
alias nmlist="sudo nmcli con list"
alias nmdev="sudo nmcli dev"

#SSH
alias ssh.add="ssh-add $HOME/.ssh/id_{rsa{,_tu},ed25519{,_aur,_github,_holoplot}}"
alias ssh.new='ssh-keygen -t ed25519 -C "$(whoami)@$(hostname)-$(date -I)"'
if [ $VENDOR = "apple" ]; then
  # apple's date is broken
  alias ssh.new='ssh-keygen -t ed25519 -C "$(whoami)@$(hostname)-$(date +%Y-%m-%d)"'
fi

#KEYCHAIN
alias keyflush="keychain --clear && keychain -k mine"

#PYTHON
alias pyserv='python -m http.server' # start a python http server

# VDIRSYNCER
alias vsync="vdirsyncer sync"

## OVERRIDES
# Colorize grep output
alias grep='grep --color=auto --exclude-dir=.git'
alias exit="clear; exit"
alias .='source'

# packaging
alias db-update='ssh repos.archlinux.org "/packages/db-update && /community/db-update"'
