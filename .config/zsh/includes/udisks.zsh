# EXAMPLES:
# - mounting devices
#    sdm sdb1       // mount first partition on sdb
#    sdm sdb 1      // mount first partition on sdb
#    sdm sdb        // mount all available partitions on sdb
#    sdm            // mount all available devices
#    mcm mmcblk0p1  // mount first partition on mmcblk0
#    mcm mmcblk0 1  // mount first partition on mmcblk0
#    mcm mmcblk0    // mount all partitions on mmcblk0
#    mcm            // mount all partitions on all mmcblk devices
#    srm 0          // mount first optical drive
#    srm            // mount all optical drives
#
# - unmounting devices
#    sdu sdb1       // unmount first partition on sdb
#    sdu sdb 1      // unmount first partition on sdb
#    sdu sdb        // unmount all available partitions on sdb
#    sdu            // unmount all available devices
#    mcu mmcblk0p1  // unmount first partition on mmcblk0
#    mcu mmcblk0 1  // unmount first partition on mmcblk0
#    mcu mmcblk0    // unmount all partitions on mmcblk0
#    mcu            // unmount all partitions on all mmcblk devices
#    sru 0          // unmount first optical drive
#    sru            // unmount all optical drives
#

# mount commands:
mount="udisksctl mount -b"
unmount="udisksctl unmount -b"

# Path to all devices
folder="/dev/"

# Devices of the form /dev/sd{b,c,d,e,f,g,h,i,j,k,l,m,n}
# path
dev="/dev/sd"
# Device numbers/names
devices=( b c d e f g h i j k l m n )
# Partition numbers
device_partitions=( 1 2 3 4 5 6 7 8 9 )

# Devices of the form /dev/mmcblk{0,1,2,3,4,5,6,7,8,9}p{1,2,3,4,5,6,7,8,9}
# path
mmc="/dev/mmcblk"
# Device numbers/names
mmc_devices=( 0 )
# Partition numbers
mmc_partitions=( p1 p2 p3 p4 p5 p6 p7 p8 p9 )

# Devices of the form /dev/sr{0,1,2}
# path
optical="/dev/sr"
# Device numbers/names
optical_devices=( 0 1 2 )

# device functions:
sdm() {
  if [[ -n "$2" ]]; then # if 2nd argument is set
    if [[ "$1" = ?d* ]]; then # check if 1st argument features the letter d
      if [[ -b $folder$1$2 ]]; then # check if block device $1$2 is available
        $=mount $folder$1$2
      fi
    fi
  elif [[ -n "$1" ]]; then # if 1st argument is set
    if [[ "$1" = ?d* ]]; then # check if 1st argument featurs a letter d
      if [[ -b $folder$1"1" ]]; then #check if first device is available
          for a in $device_partitions; do # loop through device partitions of first argument
            echo "$folder$1$a"
            if [[ -b $folder$1$a ]]; then # checking for each device if available and block device
              $=mount "$folder$1$a"
            fi
          done
      else
        echo "Trying to mount: $folder$1"
        if [[ -b $folder$1 ]]; then # check if 1st argument already is the block device to mount
          $=mount $folder$1
        fi
      fi
    fi
  else # here we loop all available block devices and mount them
    # TODO: check with df if the device is already mounted
    for a in $devices; do # loop through the list of all devices
      if [[ -b $dev$a ]]; then # check if first partition of block device is available
        for b in $device_partitions; do # loop through the list of all partitions
          if [[ -b $dev$a$b ]]; then # if it's a block device
            echo "Trying to mount: $dev$a$b"
            $=mount "$dev$a$b"
          fi
        done
      fi
    done
  fi
}

sdu() {
  if [[ -n "$2" ]]; then
    if [[ "$1" = ?d* ]]; then
      if [[ -b $folder$1$2 ]]; then
        echo "Trying to unmount: $folder$1$2"
        $=unmount $folder$1$2
      fi
    fi
  elif [[ -n "$1" ]]; then
    if [[ "$1" = ?d* ]]; then
      if [[ -b $folder$1"1" ]]; then
          for a in $device_partitions; do
            if [[ -b $folder$1$a ]]; then # checking for each device if available and block device
              echo "Trying to unmount: $folder$1$a"
              $=unmount $folder$1$a
            fi
          done
      else
        if  [[ -b $folder$1 ]]; then
          echo "Trying to unmount: $folder$1"
          $=unmount $folder$1
        fi
      fi
    fi
  else # unmount all devices mounted
    for a in $devices; do
      if [[ -b $dev$a ]]; then
        for b in $device_partitions; do
          if [[ -b $dev$a$b ]]; then # if it's a block device
            echo "Trying to unmount: $dev$a$b"
            $=unmount $dev$a$b
          fi
        done
      fi
    done
  fi
}

# mmc functions:
mcm() {
  if [[ -n "$2" ]]; then
    if [[ "$1" = mmcblk* ]]; then
      if [[ -b $folder$1"p"$2 ]]; then
        echo "Trying to mount: $folder$1"p"$2"
        $=mount $folder$1"p"$2
      fi
    fi
  elif [[ -n "$1" ]]; then
    if [[ "$1" = mmcblk* ]]; then
      if [[ -b $folder$1"p1" ]]; then
          for a in $mmc_partitions; do
            if [[ -b $folder$1$a ]]; then
              echo "Trying to mount: $folder$1$a"
              $=mount $folder$1$a
            fi
          done
      else
        if  [[ -b $folder$1 ]]; then
          echo "Trying to mount: $folder$1"
          $=mount $folder$1
        fi
      fi
    fi
  else
    for a in $mmc_devices; do
      if [[ -b $mmc$a"p1" ]]; then
        for b in $mmc_partitions; do
          if [[ -b $mmc$a$b ]]; then # if it's a block device
            echo "Trying to mount: $mmc$a$b"
            $=mount $mmc$a$b
          fi
        done
      fi
    done
  fi
}

mcu() {
  if [[ -n "$2" ]]; then
    if [[ "$1" = mmcblk* ]]; then
      if [[ -b $folder$1"p"$2 ]]; then
        echo "Trying to unmount: $folder$1"p"$2"
        $=unmount $folder$1"p"$2
      fi
    fi
  elif [[ -n "$1" ]]; then
    if [[ "$1" = mmcblk* ]]; then
      if [[ -b $folder$1"p1" ]]; then
          for a in $mmc_partitions; do
            echo "Trying to unmount: $folder$1$a"
            $=unmount $folder$1$a
          done
      else
        if [[ -b $folder$1 ]]; then
          echo "Trying to unmount: $folder$1"
          $=unmount $folder$1
        fi
      fi
    fi
  else
    for a in $mmc_devices; do
      if [[ -b "$mmc$a${mmc_partitions[0]}" ]]; then
        echo $mmc$a${mmc_partitions[0]}
        for b in $mmc_partitions; do
          if [[ -b $mmc$a$b ]]; then # if it's a block device
            echo "Trying to unmount: $mmc$a$b"
            $=unmount $mmc$a$b
          fi
        done
      fi
    done
  fi
}

# sr functions:
srm() {
  if [[ -n "$1" ]]; then
    if [[ "$1" = ? ]]; then
      if [[ -b $optical$1 ]]; then
        echo "Trying to mount: $optical$1"
        $=mount $optical$1
      fi
    fi
  else
    for a in $optical_devices; do
      if [[ -b $optical$a ]]; then
        echo "Trying to mount: $optical$a"
        $=mount $optical$a
      fi
    done
  fi
}

sru() {
  if [[ -n "$1" ]]; then
    if [[ "$1" = ? ]]; then
      if [[ -b $optical$1 ]]; then
        echo "Trying to unmount: $optical$1"
        $=unmount $optical$1
      fi
    fi
  else
    for a in $optical_devices; do
      if [[ -b $optical$a ]]; then
        echo "Trying to unmount: $optical$a"
        $=unmount $optical$a
      fi
    done
  fi
}

