if [[ -n $TMUX ]] && [[ $VENDOR != "apple" ]] && [[ ! -e /etc/debian_version ]]; then
  export TERM="tmux-256color"
elif [[ -n $STY ]];then
  export TERM="screen-256color"
else
  export TERM="xterm-256color"
fi


available_locale=$(locale -a|tr '\n' ' ')
if [ "${available_locale#*en_DK.utf8}" != "$available_locale" ]; then
    export LC_TIME="en_DK.UTF-8"
elif [ "${available_locale#*en_DK.UTF-8}" != "$available_locale" ]; then
    export LC_TIME="en_DK.UTF-8"
fi
if [ "${available_locale#*de_DE.utf8}" != "$available_locale" ]; then
    export LC_MONETARY="de_DE.UTF-8"
elif [ "${available_locale#*de_DE.UTF-8}" != "$available_locale" ]; then
    export LC_MONETARY="de_DE.UTF-8"
fi
if [ "${available_locale#*en_US.utf8}" != "$available_locale" ]; then
    export LANG="en_US.UTF-8"
elif [ "${available_locale#*en_US.UTF-8}" != "$available_locale" ]; then
    export LANG="en_US.UTF-8"
fi
if [ "${available_locale#*en_US.utf8}" != "$available_locale" ] ; then
    export LC_CTYPE="en_US.UTF-8"
elif [ "${available_locale#*en_US.UTF-8}" != "$available_locale" ]; then
    export LC_CTYPE="en_US.UTF-8"
fi

# cargo
export CARGO_TARGET_DIR="$HOME/.cache/cargo_target_dir/"
export CARGO_HOME="$HOME/.local/state/cargo/"

# rustup
export RUSTUP_HOME="$HOME/.local/state/rustup"

# keyboard exports for sway
export XKB_DEFAULT_LAYOUT=de

# browser
export BROWSER=firefox

# systemd
export SYSTEMD_EDITOR=helix

# editor
export EDITOR=helix
export VISUAL=helix

# if vimpager is installed, use it instead of less
if [[ -x /usr/bin/vimpager ]];then
  export PAGER="vimpager"
else
  export PAGER="less -j4"
fi

export GREP_COLORS="1;33"

# let RVM ignore global ruby
export rvm_ignore_gemrc_issues=1

# Java
# font settings/ renderings
export _JAVA_OPTIONS='-Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true'
# non-reparenting windows
export _JAVA_AWT_WM_NONREPARENTING=1

# pipenv environment variables:
# - disabling fancy shell stuff
# - creating virtualenv within each project
# - moving cache dir to build volume
export PIPENV_NOSPIN="true"
export PIPENV_COLORBLIND="true"
export PIPENV_HIDE_EMOJIS="true"
export PIPENV_VENV_IN_PROJECT="true"
#export PIPENV_CACHE_DIR="$HOME/.cache/pipenv"

# pyenv

export PYENV_ROOT="$HOME/.local/share/pyenv"

# brew
if command -v brew > /dev/null; then
  export HOMEBREW_NO_ANALYTICS="true"
  export HOMEBREW_NO_AUTO_UPDATE="true"
  export HOMEBREW_NO_EMOJI="true"
fi

# GnuPG
export GPG_TTY=$(tty)
export GPG_AGENT_INFO=""

# ssh-agent
if [[ $UID -ne 0 ]] && [[ $VENDOR != "apple" ]];then
  export SSH_AGENT_PID=""
  export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/gnupg/S.gpg-agent.ssh"
#  export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
fi

# tmux
if [[ $UID -ne 0 ]] && [[ $VENDOR != "apple" ]]; then
  export TMUX_TMPDIR="$XDG_RUNTIME_DIR/"
else
  export TMUX_TMPDIR="/tmp"
fi

# zsh
if [[ $UID -eq 0 ]]; then
  export TMOUT=360
  readonly TMOUT
  export TMOUT
fi
