-- color
vim.o.termguicolors = true

-- Lines
vim.o.number = true -- show line numbers
vim.o.history = 10000 -- store 10k lines of :cmd history
vim.o.showtabline = 2 -- Always show tab pages line
vim.o.showmode = false -- Do not echo the mode, status line will display it
vim.opt.wildmode:prepend { 'longest:full' } -- Incremental command-line completions
vim.opt.cursorline = true
vim.o.nowrap = true
vim.o.linebreak = true

-- Search
vim.o.ignorecase = true -- Ignore case
vim.o.smartcase = true -- .. unless "pattern contains upper case characters"

-- indentation
vim.o.autoindent = true
vim.o.smartindent = true
vim.o.smarttab = true
vim.o.expandtab = true
vim.o.shiftwidth = 2
vim.o.softtabstop = 2
vim.o.tabstop = 2

-- character display
vim.o.listchars = 'tab:>-,trail:·,nbsp:+'
vim.o.list = true

-- file editing
vim.o.noswapfile = true
vim.o.nobackup = true
vim.o.nowb = true
vim.opt.swapfile = false
vim.o.undofile = true

-- ignore patterns when expanding wildcards
vim.opt.wildignore = {
  '*.o,*.obj,*~',
  '*vim/backups*',
  '*nvim/undo*',
  '*sass-cache*',
  '*DS_Store*',
  'vendor/rails/**',
  'vendor/cache/**',
  '*.gem',
  'log/**',
  'tmp/**',
  '*.png,*.jpg,*.gif',
}

-- ultisnips: https://github.com/SirVer/ultisnips
-- vim.g['UltiSnipsSnippetDirectories'] = {"~/.local/share/UltiSnips/"}
vim.g['UltiSnipsExpandTrigger'] = "<tab>"
vim.g['UltiSnipsJumpForwardTrigger'] = "<c-j>"
vim.g['UltiSnipsJumpBackwardTrigger'] = "<c-k>"

-- gopass: https://github.com/gopasspw/gopass
vim.api.nvim_create_autocmd({'BufNewFile', 'BufRead'}, {
  pattern = '/dev/shm/gopass*',
  desc = 'Disable backup, undo and swap when editing gopass secrets',
  callback = function()
    vim.api.nvim_set_option_value('swapfile', false, {['scope'] = 'local'})
    vim.api.nvim_set_option_value('backup', false, {['scope'] = 'local'})
    vim.api.nvim_set_option_value('undofile', false, {['scope'] = 'local'})
    vim.api.nvim_set_option_value('shada', '', {['scope'] = 'local'})
  end
})

-- ale: https://github.com/dense-analysis/ale
vim.g['ale_completion_enabled'] = 1
vim.g['ale_linters'] = {
  ['rust'] = {'rustc', 'analyzer'},
  ['python'] = {'bandit', 'flake8', 'mypy', 'pydocstyle', 'pyright'},
}
vim.opt.omnifunc = 'ale#completion#OmniFunc'

-- scnvim: https://github.com/davidgranstrom/scnvim

-- Diagnostic mappings
local opts = { noremap = true, silent = true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

vim.api.nvim_create_autocmd('LspAttach', {
  callback = function(args)
    local bufnr = args.buf
    local client = vim.lsp.get_client_by_id(args.data.client_id)
    local capabilities = client.server_capabilities

    -- LSP mappings
    local lsp_formatting = function()
      vim.lsp.buf.format({
        bufnr = bufnr,
        filter = function()
          return client.name ~= 'sumneko_lua' or 'tsserver'
        end,
      })
    end

    local bufopts = { noremap = true, silent = true, buffer = bufnr }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
    -- if capabilities.hoverProvider and client.name ~= 'null-ls' then
    --   vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
    -- end
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
    vim.keymap.set('n', '<space>wl', function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, bufopts)
    vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
    vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
    vim.keymap.set('n', '<space>f', lsp_formatting, bufopts)

    -- Capabilities checks
    if capabilities.colorProvider then
      require('document-color').buf_attach(bufnr)
    end

    if capabilities.documentHighlightProvider then
      vim.api.nvim_create_augroup('lsp_document_highlight', {
        clear = false,
      })
      vim.api.nvim_clear_autocmds({
        buffer = bufnr,
        group = 'lsp_document_highlight',
      })
      vim.api.nvim_create_autocmd({ 'CursorHold', 'CursorHoldI' }, {
        group = 'lsp_document_highlight',
        buffer = bufnr,
        callback = vim.lsp.buf.document_highlight,
      })
      vim.api.nvim_create_autocmd('CursorMoved', {
        group = 'lsp_document_highlight',
        buffer = bufnr,
        callback = vim.lsp.buf.clear_references,
      })
    end

    if capabilities.signatureHelpProvider then
      require('lsp_signature').on_attach()
    end
  end,
})

-- lsp_inlayhints needs release: https://github.com/lvimuser/lsp-inlayhints.nvim/issues/29
-- vim.api.nvim_create_augroup('LspAttach_inlayhints', {})
-- vim.api.nvim_create_autocmd('LspAttach', {
--   group = 'LspAttach_inlayhints',
--   callback = function(args)
--     if not (args.data and args.data.client_id) then
--       return
--     end
-- 
--     local bufnr = args.buf
--     local client = vim.lsp.get_client_by_id(args.data.client_id)
--     require('lsp-inlayhints').on_attach(client, bufnr)
--   end,
-- })

-- local on_attach = function(client)
--     require'completion'.on_attach(client)
-- end

local lsp_flags = {
  debounce_text_changes = 150,
}
require('lspconfig')['pyright'].setup{
    on_attach = on_attach,
    flags = lsp_flags,
}
require('lspconfig')['tsserver'].setup{
    on_attach = on_attach,
    flags = lsp_flags,
}

require('lspconfig')['rust_analyzer'].setup{
    on_attach = on_attach,
    flags = lsp_flags,
    settings = {
        ["rust-analyzer"] = {
            imports = {
                granularity = {
                    group = "module",
                },
                prefix = "self",
            },
            cargo = {
                buildScripts = {
                    enable = true,
                },
            },
            procMacro = {
                enable = true
            },
        }
    }
}

-- treesitter: https://github.com/nvim-treesitter/nvim-treesitter
vim.opt.runtimepath:append("~/.local/state/nvim/parsers/")
require'nvim-treesitter.configs'.setup {
  ensure_installed = {
    "bash",
    "c",
    "cmake",
    "cpp",
    "diff",
    "git_rebase",
    "gitattributes",
    "gitcommit",
    "gitignore",
    "go",
    "html",
    "latex",
    "lua",
    "make",
    "markdown",
    "meson",
    "ninja",
    "php",
    "python",
    "regex",
    "rst",
    "rust",
    -- "sql",
    "supercollider",
    -- "toml",
    "typescript",
    "vim",
    "yaml",
    "zig",
  },
  sync_install = false,
  auto_install = false,
  ignore_install = { "javascript" },
  parser_install_dir = "~/.local/state/nvim/parsers/",
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
}
