if(PathName("/dev/fw1").isFile, {
  if(("command -v ffado-test > /dev/null").systemCmd == 0, {
    ~ffadoDevice = ("ffado-test ListDevices 2>&1|tail -1").unixCmdGetStdOut;
    if(~ffadoDevice.contains("Fireface 800"), {
      ~audioInterfaceOptions = Dictionary.with(*[
        \name->'RME Fireface 800',
        \numInputs->28,
        \numOutputs->28,
        \inputs->Dictionary.with(*[
          \adat1->Array.series(8, 12, 1),
          \adat2->Array.series(8, 20, 1),
          \analog->Array.series(10, 0, 1),
          \spdif->Array.series(2, 8, 1)
        ]),
        \outputs->Dictionary.with(*[
          \adat1->Array.series(8, 12, 1),
          \adat2->Array.series(8, 20, 1),
          \analog->Array.series(8, 0, 1),
          \headphones1->Array.series(2, 8, 1),
          \spdif->Array.series(2, 10, 1)
        ])
      ]);
      "SC_JACK_DEFAULT_INPUTS".setenv("firewire_pcm");
      "SC_JACK_DEFAULT_OUTPUTS".setenv("firewire_pcm");
    });
  });
},{
  if(("which aplay > /dev/null").systemCmd == 0, {
    ~alsaDevices = ("aplay -l").unixCmdGetStdOut;
    // case defining name, number of input and output channels and a Dictionary
    // holding Symbols for accessing groups of inputs and outputs by name
    ~audioInterfaceOptions = case
      {
        ~alsaDevices.contains("Babyface")
      }{
        Dictionary.with(*[
          \name->'RME Babyface',
          \numInputs->10,
          \numOutputs->12
        ])
      }{
        ~alsaDevices.contains("XUSB")
      }{
        Dictionary.with(*[
          \name->'Behringer XUSB',
          \numInputs->32,
          \numOutputs->32
        ])
      }{
        ~alsaDevices.contains("Scarlett 18i20")
      }{
        Dictionary.with(*[
          \name->'Focusrite Scarlett 18i20',
          \numInputs->18,
          \numOutputs->20,
          \inputs->Dictionary.with(*[
            \analog1->Array.series(8, 0, 1),
            \spdif->Array.series(2, 8, 1),
            \adat1->Array.series(8, 10, 1)
          ]),
          \outputs->Dictionary.with(*[
            \analog1->Array.series(10, 0, 1),
            \headphones1->Array.series(2, 6, 1),
            \headphones2->Array.series(2, 8, 1),
            \spdif->Array.series(2, 10, 1),
            \adat1->Array.series(8, 12, 1)
          ])
        ])
      }{
        ~alsaDevices.contains("Fireface UFX ")
      }{
        Dictionary.with(*[
          \name->'RME Fireface UFX',
          \numInputs->22,
          \numOutputs->22,
          \inputs->Dictionary.with(*[
            \adat1->Array.series(8, 14, 1),
            \adat2->Array.series(8, 0, 1),
            \analog->Array.series(12, 0, 1),
            \spdif->Array.series(2, 12, 1)
          ]),
          \outputs->Dictionary.with(*[
            \adat1->Array.series(8, 14, 1),
            \adat2->Array.series(8, 0, 1),
            \analog->Array.series(8, 0, 1),
            \headphones1->Array.series(2, 8, 1),
            \headphones2->Array.series(2, 10, 1),
            \spdif->Array.series(2, 12, 1)
          ])
        ])
      }{
        ~alsaDevices.contains("GRAILUltraLite")
      }{
        Dictionary.with(*[
          \name->'Motu UltraLite AVB',
          \numInputs->24,
          \numOutputs->24,
          \inputs->Dictionary.with(*[
            \adat1->Array.series(8, 10, 1),
            \analog1->Array.series(10, 0, 1),
          ]),
          \outputs->Dictionary.with(*[
            \adat1->Array.series(8, 10, 1),
            \analog1->Array.with(0,1,4,5,6,7),
            \headphones1->Array.with(2,3)
          ])
        ])
      }{
        ~alsaDevices.contains("PCH")
      }{
        Dictionary.with(*[
          \name->'Internal',
          \numInputs->2,
          \numOutputs->2
        ])
      };
  });
});
~additionalChannels = Dictionary.with(*[\inputs -> Dictionary.with(*[]), \outputs -> Dictionary.with(*[])]);
Server.local.options.numInputBusChannels = ~audioInterfaceOptions.at(\numInputs);
Server.local.options.numOutputBusChannels = ~audioInterfaceOptions.at(\numOutputs);
postln("Loaded settings for: "++~audioInterfaceOptions.at(\name));
postln("Additional channels: "++~additionalChannels.at(\inputs).values++"/ "++~additionalChannels.at(\outputs).values);

//postln("Initializing and connecting MIDI devices.");
//MIDIClient.init;
//MIDIIn.connectAll;

//TODO: use Quarks interface to install/update needed Quarks instead of symlinking.
if (File.exists(Platform.userConfigDir++"/synthdefs.scd"), {
  File.readAllString(Platform.userConfigDir++"/synthdefs.scd").interpret;
});

if (File.exists(Platform.userConfigDir++"/patterns.scd"), {
  File.readAllString(Platform.userConfigDir++"/patterns.scd").interpret;
});

if (File.exists(Platform.userConfigDir++"/functions.scd"), {
  File.readAllString(Platform.userConfigDir++"/functions.scd").interpret;
});
